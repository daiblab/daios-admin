var express = require('express');
var router = express.Router();
var coins = require('../backend/controller/coins');
var sessionChecker = require('../utils/session');

router.get('/lists', sessionChecker.sessionChecker, function(req, res, next) {
    res.render('users/list', { title: 'Biteweb Admin - Users' });
});

router.post('/search', function(req, res, next) {
    coins.listCoins(req, res);
});

router.get('/:coinId', function(req,res, next) {
    coins.getCoinDaiWithdrawHistory(req, res);
})

router.put('/:coinId', function(req,res, next) {
    coins.updateCoinWithdrawHistory(req, res);
})

router.put('/withdraw/:historyId', function(req,res, next) {
    coins.updateCoinDaiWithdrawHistory(req, res);
})


module.exports = router;