var express = require('express');
var router = express.Router();
var user = require('../backend/controller/user');
var sessionChecker = require('../utils/session');

router.get('/lists', sessionChecker.sessionChecker, function(req, res, next) {
    res.render('users/list', { title: 'Biteweb Admin - Users' });
});

router.post('/search', function(req, res, next) {
    user.listUsers(req, res);
});

module.exports = router;