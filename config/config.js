module.exports = {
    db: {
        mongodb_user: 'dev:daiblab123@',
        mongodb_url: 'localhost:27017',
        db_name: 'daios'
    },
    mqtt: {
        url: "mqtt://localhost",                              //mqtt://35.189.153.47
    },
    mailer: {
        service: 'Gmail',
        host: 'localhost',
        port: '465',
        user: 'mailer@daiblab.com',
        password: 'daiblab123',
    },
    testnet: {
        address: {
            btc: '2NGPYngrvKXk5sJDZa2rwT1pt45vAfHBi8R',
            ether: '0xD2c750DEef3A0f577Dd00dF2F83a56555cE47F3e',
            mach: '0xD2c750DEef3A0f577Dd00dF2F83a56555cE47F3e',
        },
        contract: {
            mach: '0xd81623dbaf0da6fd191d271edbf1a19ca9775529'
        },
        privateKey: {
            btc: "",
            ether: "60DFDD0731969584EAECC73BE339414506BE2F74A4E68C546AC13032F50F6079",
            mach: ""
        }
    },
    mainnet: {
        address:{
            btc: '3DXsgHHB17yCYBnRtPKeXVsKT9G5ohkpB1',
            ether: '0x8dcD316c8c1Ca566030063E7cd62D2322223e494',
            mach: '0x8dcD316c8c1Ca566030063E7cd62D2322223e494',
        },
        contract: {
            mach: '0xc23447ba70a10cd5ce4b8e476166e17ccd49bfd4'
        }
    }
};