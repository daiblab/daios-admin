function getAjaxMethod(url, callback) {
    $.ajax({
        method: "GET",
        url: url,
        contentType: "application/json; charset=utf-8"
    })
    .done(function (success) {
        callback(success);
    })
    .fail(function (fail) {
        console.log(fail);
    });
}

function postAjaxMethod(url, param, callback) {
    $.ajax({
        method: "POST",
        url: url,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(param)
    })
    .done(function (success) {
        callback(success);
    })
    .fail(function (fail) {
        console.log(fail);
    });
}

function putAjaxMethod(url, param, callback) {
    $.ajax({
        method: "PUT",
        url: url,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(param)
    })
    .done(function (success) {
        callback(success);
    })
    .fail(function (fail) {
        console.log(fail);
    });
}

function deleteAjaxMethod(url, callback) {
    $.ajax({
        method: "DELETE",
        url: url,
        contentType: "application/json; charset=utf-8"
    })
    .done(function (success) {
        callback(success);
    })
    .fail(function (fail) {
        console.log(fail);
    });
}