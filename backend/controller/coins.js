var BitwebResponse = require('../../utils/BitwebResponse');
var serviceCoins = require('../service/coins');
var mqtt = require('../../utils/mqtt');
// let ethereumWeb3 = require('../../utils/ethereumWeb3');

function listCoins(req, res) {
    var bitwebResponse = new BitwebResponse();

    serviceCoins.listCoins(req)
        .then(result => {
            bitwebResponse.code = 200;
            bitwebResponse.data = result;
            res.status(200).send(bitwebResponse.create());
        }).catch((err) => {
        console.error('err=>', err)
        bitwebResponse.code = 500;
        bitwebResponse.message = err;
        res.status(500).send(bitwebResponse.create())
    })
}

function getCoinDaiWithdrawHistory(req, res) {
    var bitwebResponse = new BitwebResponse();
    let body = {
        "coinId":req.params.coinId,
        lockup: { $exists: true }
    }

    serviceCoins.getCoinDaiWithdrawHistory(body)
        .then(result => {
            bitwebResponse.code = 200;
            bitwebResponse.data = result;
            res.status(200).send(bitwebResponse.create());
        }).catch((err) => {
            console.error('err=>', err)
            bitwebResponse.code = 500;
            bitwebResponse.message = err;
            res.status(500).send(bitwebResponse.create())
        })
}

function updateCoinWithdrawHistory(req, res) {
    /**
     * coinType: btc, ether, dai
     * price : dai를 btc, ether, dai로 환산된 금액 (유저가 받아갈 금액)
     * dai : 출금 요청할 dai
     * address : user address
     * */
    let sampleJson =
        {
            "price": 10,
            "dai": 20,
            "address": "0xdC76866e2457a7fafB7E55BbB84b14C4DdCCA42c"
        }

    var daiosResponse = new BitwebResponse();
    if ((req.body.coinId != null) && (req.body.coinType != null)) {

        let coinId = req.body.coinId;
        let coinType = req.body.coinType;
        let address = req.body.address;
        let dai = req.body.dai;
        let price = req.body.price;
        let status = req.body.status;

        if (coinType == "ether" || coinType == "dai") {

            let ethereumWeb3 = require('../../utils/ethereumWeb3');

            let data = {}
            data['type'] = coinType;
            data['price'] = price;
            data['address'] = address;

            ethereumWeb3.withDraw(data)
                .then(transaction => {
                    console.log('withDraw() =>', transaction);
                    let data = {}
                    data['coinType'] = coinType;
                    data['dai'] = dai;
                    data['price'] = price;
                    data['completedPrice'] = price;
                    data['address'] = address;
                    data['transaction'] = transaction;
                    data['status'] = status;

                    serviceCoins.createCoinWithdrawHistoryByCoinId(coinId, data)
                        .then(result => {

                            let historyId = result._id;
                            console.log('historyId->', historyId);

                            serviceCoins.updateWithWithdrawHistory(coinType, coinId, historyId)
                                .then((result1) => {
                                    serviceCoins.updateCoinHistoryWithdrawStatusById(req.body.fromCoinType, req.body.historyId, historyId)
                                        .then((result2) => {
                                            serviceCoins.getByCoinId(coinId)
                                                .then(result3 => {
                                                    console.log('getCoin result=>', result);

                                                    // let jsonData = {}
                                                    // jsonData['coinId'] = coinId;
                                                    // jsonData['historyId'] = historyId;
                                                    // jsonData['type'] = coinType + "_withdraw";
                                                    // jsonData['price'] = price;
                                                    // jsonData['address'] = address;
                                                    // jsonData['transaction'] = transaction;
                                                    // jsonData['dai'] = dai;
                                                    // jsonData['total_dai'] = result.total_dai;
                                                    //
                                                    // mqtt.WithdrawJob(jsonData)

                                                    daiosResponse.code = 200;
                                                    daiosResponse.data = result;
                                                    res.status(200).send(daiosResponse.create())

                                                }).catch(err => {
                                                console.log('err=>', err);
                                            })
                                        })
                                }).catch((err => {
                                console.error('err=>', err)
                            }))
                        })
                })
        } else if (coinType == "btc") {
            let bitcore_lib = require('../utils/bitcore_lib');
            bitcore_lib.sendBtcTransaction(req.body)
                .then(transaction => {
                    console.log('sendBtcTransaction() =>', transaction);
                    let data = {}
                    data['coinType'] = coinType;
                    data['dai'] = dai;
                    data['price'] = price;
                    data['address'] = address;
                    data['transaction'] = transaction;

                    serviceCoins.createCoinWithdrawHistoryByCoinId(coinId, data)
                        .then(result => {

                            let historyId = result._id;
                            console.log('historyId->', historyId);

                            serviceCoins.updateWithWithdrawHistory(coinType, coinId, historyId)
                                .then((result) => {
                                    serviceCoins.getByCoinId(coinId)
                                        .then(result => {
                                            console.log('getCoin result=>', result);

                                            // let jsonData = {}
                                            // jsonData['coinId'] = coinId;
                                            // jsonData['historyId'] = historyId;
                                            // jsonData['type'] = coinType + "_withdraw";
                                            // jsonData['price'] = price;
                                            // jsonData['address'] = address;
                                            // jsonData['transaction'] = transaction;
                                            // jsonData['dai'] = dai;
                                            // jsonData['total_dai'] = result.total_dai;
                                            //
                                            // mqtt.WithdrawBtcJob(jsonData)

                                            daiosResponse.code = 200;
                                            daiosResponse.data = result;
                                            res.status(200).send(daiosResponse.create())

                                        }).catch(err => {
                                        console.log('err=>', err);
                                    })

                                }).catch((err => {
                                console.error('err=>', err)
                            }))
                        })
                })

        } else {
            console.log("coinType undefined!")
        }
    }
}

function updateCoinDaiWithdrawHistory(req, res) {
    var bitwebResponse = new BitwebResponse();
    let historyId = req.params.historyId;
    let body = {
        "lockup":req.body.lockup
    }

    serviceCoins.updateCoinDaiWithdrawHistory(historyId, body)
        .then(result => {
            bitwebResponse.code = 200;
            bitwebResponse.data = result;
            res.status(200).send(bitwebResponse.create());
        }).catch((err) => {
        console.error('err=>', err)
        bitwebResponse.code = 500;
        bitwebResponse.message = err;
        res.status(500).send(bitwebResponse.create())
    })
}

exports.listCoins = listCoins;
exports.updateCoinWithdrawHistory = updateCoinWithdrawHistory;
exports.getCoinDaiWithdrawHistory = getCoinDaiWithdrawHistory;
exports.updateCoinDaiWithdrawHistory = updateCoinDaiWithdrawHistory;