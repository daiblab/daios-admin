var BitwebResponse = require('../../utils/BitwebResponse');
var serviceUser = require('../service/user');


function listUsers(req, res) {
    var bitwebResponse = new BitwebResponse();

    serviceUser.listUsers(req)
        .then(result => {
            bitwebResponse.code = 200;
            bitwebResponse.data = result;
            res.status(200).send(bitwebResponse.create());
        }).catch((err) => {
        console.error('err=>', err)
        bitwebResponse.code = 500;
        bitwebResponse.message = err;
        res.status(500).send(bitwebResponse.create())
    })
}

exports.listUsers = listUsers;