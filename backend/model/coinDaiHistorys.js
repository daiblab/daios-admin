var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
var coinDaiHistorysSchema = require('../dao/coinDaiHistorys');

module.exports = mongoose.model('CoinDaiHistorys', coinDaiHistorysSchema);
