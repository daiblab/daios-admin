var db = require('../../utils/db');
var bitwebUser = require('./impl/user');

function listUsers(req) {
    let country = req.body.country == undefined ? "KR" : req.body.country;
    let userTag = req.body.userTag;
    let perPage = req.body.perPage == undefined ? 20 : req.body.perPage;
    let pageIdx = req.body.pageIdx == undefined ? 0 : req.body.pageIdx;
    let data = {
        "perPage": perPage,
        "pageIdx": pageIdx
    }
    let body = {};
    if(userTag !== undefined) {
        body = {
            $and: [ {"userTag": userTag} ]
        };
    }

    return new Promise((resolve, reject) => {
        db.connectDB(country)
            .then(() => bitwebUser.listUsers(body, data))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })
}

function getByUserTag(userTag) {
    return new Promise((resolve, reject) => {
        db.connectDB()
            .then(() => bitwebUser.getByUserTag(userTag))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })
}

exports.listUsers = listUsers;
exports.getByUserTag = getByUserTag;
