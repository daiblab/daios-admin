var User = require('../../model/user');

function listUsers (body, data) {
    return new Promise((resolve, reject) => {
        User.find(body)
            .limit(data.perPage)
            .skip(data.pageIdx * data.perPage)
            .sort({regDate: 'desc'})
            .exec(function (err, item) {
                if (err) {
                    console.error(err)
                    reject(err)
                }
                console.log('getUsersAll done: ' + item)
                resolve(item)
            })
    })
}

function listUsers2 () {
    return new Promise((resolve, reject) => {
        User.find({})
            .sort({regDate: 'asc'})
            .exec(function (err, item) {
                if (err) {
                    console.error(err)
                    reject(err)
                }
                //console.log('getUsersAll done: ' + item)
                resolve(item)
            })
    })
}

function getUserByPointIds(ids) {
    return new Promise((resolve, reject) => {
        User.find({
            "pointId": {$in: ids}
        })
        .exec(function (err, users) {
            if (err) {
                console.error(err)
                reject(err)
            }
            console.log('getUserByPointIds done: ' + users)
            resolve(users)
        })
    })
}

function getByUserId (userId) {
    return new Promise((resolve, reject) => {
        User.findOne(
            {"_id": userId},
            function(err, user) {
                if (err) {
                    console.error(err)
                    reject(err)
                }
                console.log('getByUserId done: ' + user)
                resolve(user)
            }
        )
    })
}

exports.listUsers = listUsers;
exports.listUsers2 = listUsers2;
exports.getUserByPointIds = getUserByPointIds;
exports.getByUserId = getByUserId;
