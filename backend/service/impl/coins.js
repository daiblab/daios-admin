var Coins = require('../../model/coins');
var CoinBtcHistorys = require('../../model/coinBtcHistorys');
var CoinEtherHistorys = require('../../model/coinEtherHistorys');
var CoinBtcWithdrawHistory = require('../../model/coinBtcWithdrawHistorys');
var CoinDaiWithdrawHistory = require('../../model/coinDaiWithdrawHistorys');
var CoinEtherWithdrawHistory = require('../../model/coinEtherWithdrawHistorys');

function listCoinHistory(coinType) {
    return new Promise((resolve, reject) => {
        if(coinType == 'btc') {
            CoinBtcHistorys.find({})
                .sort({regDate: 'asc'})
                .exec(function (err, item) {
                    if (err) {
                        console.error(err)
                        reject(err)
                    }
                    //console.log('listCoins btc done: ' + item)
                    resolve(item)
                })
        } else {
            CoinEtherHistorys.find()
                .sort({regDate: 'asc'})
                .exec(function (err, item) {
                    if (err) {
                        console.error(err)
                        reject(err)
                    }
                    //console.log('listCoins eth done: ' + item)
                    resolve(item)
                })
        }

    })
}

function getCoinDaiWithdrawHistory(data) {
    return new Promise((resolve, reject) => {
        CoinDaiWithdrawHistory.find(data)
            .sort({regDate: 'asc'})
            .exec(function (err, item) {
                if (err) {
                    console.error(err)
                    reject(err)
                }
                //console.log('listCoins btc done: ' + item)
                resolve(item)
            })
    })
}

function listCoins() {
    return new Promise((resolve, reject) => {
        Coins.find({})
            .exec(function (err, coin) {
                if (err) {
                    console.error(err)
                    reject(err)
                }
                //console.log('listCoins done: ' + coin)
                resolve(coin)
            })
    })
}

function getCoinById(coinId) {
    return new Promise((resolve, reject) => {
        Coins.findOne({"_id":coinId})
            .exec(function (err, coin) {
                if (err) {
                    console.error(err)
                    reject(err)
                }
                console.log('getCoinById done: ' + coin)
                resolve(coin)
            })
    })
}


function updateCoinById(coinId, body) {
    return new Promise((resolve, reject) => {
        Coins.findOneAndUpdate(
            {"_id": coinId
            },
            {$set: body
            },
            {upsert: false, new: true},
            function(err, data) {
                if (err) {
                    // console.error(err)
                    reject(err)
                }
                resolve(data)
            })
    })
}

function createCoinEtherWithdrawHistorys (data) {
    return new Promise((resolve, reject) => {
        console.log(data)
        var coinEtherWithdrawHistory = new CoinEtherWithdrawHistory(data)
        coinEtherWithdrawHistory.save(function (err, result) {
            if (err) {
                reject(err);
            } else {
                console.log('createCoinEtherWithdrawHistorys done: ' + result)
                resolve(result)
            }
        })
    })
}

function createCoinDaiWithdrawHistorys (data) {
    return new Promise((resolve, reject) => {
        console.log(data)
        var coinDaiWithdrawHistory = new CoinDaiWithdrawHistory(data)
        coinDaiWithdrawHistory.save(function (err, result) {
            if (err) {
                reject(err);
            } else {
                console.log('createCoinEtherWithdrawHistorys done: ' + result)
                resolve(result)
            }
        })
    })
}

function createCoinBtcWithdrawHistorys (data) {
    return new Promise((resolve, reject) => {
        console.log(data)
        var coinBtcWithdrawHistory = new CoinBtcWithdrawHistory(data)
        coinBtcWithdrawHistory.save(function (err, result) {
            if (err) {
                reject(err);
            } else {
                console.log('createCoinBtcWithdrawHistorys done: ' + result)
                resolve(result)
            }
        })
    })
}

function updateCoinEtherWithdrawHistoryId(coinId, historyId) {
    return new Promise((resolve, reject) => {

        Coins.findOneAndUpdate(
            {
                "_id": coinId
            },
            {
                $push: {ether_withdraw_historys: historyId}
            },
            {upsert: false, new: false},
            function (err, data) {
                if (err) {
                    // console.error(err)
                    reject(err)
                }
                resolve(data)
            })
    })
}

function updateCoinDaiWithdrawHistoryId(coinId, historyId) {
    return new Promise((resolve, reject) => {

        Coins.findOneAndUpdate(
            {
                "_id": coinId
            },
            {
                $push: {dai_withdraw_historys: historyId}
            },
            {upsert: false, new: false},
            function (err, data) {
                if (err) {
                    // console.error(err)
                    reject(err)
                }
                resolve(data)
            })
    })
}

function updateCoinBtcWithdrawHistoryId(coinId, historyId) {
    return new Promise((resolve, reject) => {

        Coins.findOneAndUpdate(
            {
                "_id": coinId
            },
            {
                $push: {btc_withdraw_historys: historyId}
            },
            {upsert: false, new: false},
            function (err, data) {
                if (err) {
                    // console.error(err)
                    reject(err)
                }
                resolve(data)
            })
    })
}

function updateCoinEtherWithdrawHistoryById (historyId, body) {
    return new Promise((resolve, reject) => {
        CoinEtherWithdrawHistory.findOneAndUpdate(
            {"_id": historyId
            },
            {$set: body
            },
            {upsert: false, new: true},
            function(err, data) {
                if (err) {
                    console.error(err)
                    reject(err)
                }
                resolve(data)
            })
    })
}

function updateCoinDaiWithdrawHistoryById (historyId, body) {
    return new Promise((resolve, reject) => {
        CoinDaiWithdrawHistory.findOneAndUpdate(
            {"_id": historyId
            },
            {$set: body
            },
            {upsert: false, new: true},
            function(err, data) {
                if (err) {
                    console.error(err)
                    reject(err)
                }
                resolve(data)
            })
    })
}

function updateCoinBtcWithdrawHistoryById (historyId, body) {
    return new Promise((resolve, reject) => {
        CoinBtcWithdrawHistory.findOneAndUpdate(
            {"_id": historyId
            },
            {$set: body
            },
            {upsert: false, new: true},
            function(err, data) {
                if (err) {
                    console.error(err)
                    reject(err)
                }
                resolve(data)
            })
    })
}

function updateCoinBtcWithdrawStatus(historyId, body) {
    return new Promise((resolve, reject) => {
        CoinBtcHistorys.findOneAndUpdate(
            {"_id": historyId
            },
            {$set: body
            },
            {upsert: false, new: true},
            function(err, data) {
                if (err) {
                    console.error(err)
                    reject(err)
                }
                resolve(data)
            })
    })
}

function updateCoinEtherWithdrawStatus(historyId, body) {
    return new Promise((resolve, reject) => {
        CoinEtherHistorys.findOneAndUpdate(
            {"_id": historyId
            },
            {$set: body
            },
            {upsert: false, new: true},
            function(err, data) {
                if (err) {
                    console.error(err)
                    reject(err)
                }
                resolve(data)
            })
    })
}


exports.getCoinById = getCoinById;
exports.getCoinDaiWithdrawHistory = getCoinDaiWithdrawHistory;
exports.listCoins = listCoins;
exports.listCoinHistory = listCoinHistory;
exports.updateCoinById = updateCoinById;
exports.createCoinEtherWithdrawHistorys = createCoinEtherWithdrawHistorys;
exports.createCoinDaiWithdrawHistorys = createCoinDaiWithdrawHistorys;
exports.createCoinBtcWithdrawHistorys = createCoinBtcWithdrawHistorys;
exports.updateCoinEtherWithdrawHistoryId = updateCoinEtherWithdrawHistoryId;
exports.updateCoinDaiWithdrawHistoryId = updateCoinDaiWithdrawHistoryId;
exports.updateCoinBtcWithdrawHistoryId = updateCoinBtcWithdrawHistoryId;
exports.updateCoinEtherWithdrawHistoryById = updateCoinEtherWithdrawHistoryById;
exports.updateCoinDaiWithdrawHistoryById = updateCoinDaiWithdrawHistoryById;
exports.updateCoinBtcWithdrawHistoryById = updateCoinBtcWithdrawHistoryById;
exports.updateCoinBtcWithdrawStatus = updateCoinBtcWithdrawStatus;
exports.updateCoinEtherWithdrawStatus = updateCoinEtherWithdrawStatus;