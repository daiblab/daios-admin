var db = require('../../utils/db');
var bitwebUser = require('./impl/user');
var bitwebCoins = require('./impl/coins');
var util = require('../../utils/util')

function listCoins(req) {
    let country = req.body.country == undefined ? "KR" : req.body.country;
    let coinType = req.body.coinType;
    // let perPage = req.body.perPage == undefined ? 20 : req.body.perPage;
    // let pageIdx = req.body.pageIdx == undefined ? 0 : req.body.pageIdx;
    // let data = {
    //     "perPage": perPage,
    //     "pageIdx": pageIdx
    // }
    // let body = {};
    // if(coinType !== undefined) {
    //     body = {
    //         $and: [ {"userTag": userTag} ]
    //     };
    // }

    return new Promise((resolve, reject) => {
        db.connectDB(country)
            .then(() => bitwebUser.listUsers2()
                .then((users) => {
                    for(var i in users) {
                        users[i]['_doc']['coinHistory'] = [];
                        users[i]['_doc']['daiWithdrawHistory'] = [];
                    }
                    bitwebCoins.listCoins()
                        .then((coins) => {
                            bitwebCoins.listCoinHistory(coinType)
                                .then((historys) => {
                                    bitwebCoins.getCoinDaiWithdrawHistory({})
                                        .then((withdrawHistorys) => {
                                            for(var i in users) {
                                                let selIndex = coins.findIndex((group) => {
                                                    return group._doc._id.toString() == users[i]._doc.coinId.toString();
                                                });
                                                users[i]['_doc']['coins'] = coins[selIndex];

                                                let coin_historys;
                                                if(coinType == "btc") {
                                                    coin_historys = coins[selIndex]._doc.btc_historys;
                                                } else {
                                                    coin_historys = coins[selIndex]._doc.ether_historys;
                                                }

                                                for(var j in coin_historys) {
                                                    let selIndex1 = historys.findIndex((group) => {
                                                        return group._doc._id.toString() == coin_historys[j];
                                                    });
                                                    if (selIndex1 != -1) {
                                                        users[i]['_doc']['coinHistory'].push(historys[selIndex1]);

                                                        let selIndex2 = withdrawHistorys.findIndex((group) => {
                                                            return group._doc._id.toString() == historys[selIndex1]._doc.withdraw_history_id;
                                                        });
                                                        if (selIndex2 != -1) {
                                                            users[i]['_doc']['daiWithdrawHistory'].push(withdrawHistorys[selIndex2]);
                                                        }
                                                    }
                                                }
                                            }

                                            console.log('result=>' , users);
                                            resolve(users)
                                        })
                                })
                        })
                })
            ).catch((err) => {
            reject(err)
        })
    })
}

function getCoinDaiWithdrawHistory(historyId, data) {
    return new Promise((resolve, reject) => {
        db.connectDB()
            .then(() => {
                bitwebCoins.getCoinDaiWithdrawHistory(historyId, data)
                    .then((result) => {
                        //console.log('result=>' , result);
                        resolve(result)
                    }).catch((err) => {
                    reject(err)
                })
            })
    });
}

function createCoinWithdrawHistoryByCoinId(coinId , data) {
    return new Promise((resolve, reject) => {

        data['coinId'] = coinId;
        // data['type'] = req.params.coinType;
        data['regDate'] = util.formatDate(new Date().toLocaleString('ko-KR'))
        //data['completeDate'] = "";
        //data['status'] = false;

        db.connectDB()
            .then(() => {
                if (data.coinType == "ether") {
                    bitwebCoins.createCoinEtherWithdrawHistorys(data)
                        .then((result) => {
                            console.log('result=>' , result);
                            resolve(result)
                        }).catch((err) => {
                        reject(err)
                    })
                } else if (data.coinType == "btc") {
                    bitwebCoins.createCoinBtcWithdrawHistorys(data)
                        .then((result) => {
                            console.log('result=>' , result);
                            resolve(result)
                        }).catch((err) => {
                        reject(err)
                    })
                } else if (data.coinType == "dai") {
                    bitwebCoins.createCoinDaiWithdrawHistorys(data)
                        .then((result) => {
                            console.log('result=>' , result);
                            resolve(result)
                        }).catch((err) => {
                        reject(err)
                    })
                }
            })

    })

}

function updateWithWithdrawHistory(coinType, coinId, historyId) {
    return new Promise((resolve , reject) => {

        db.connectDB()
            .then(() => {
                if (coinType == "ether") {
                    bitwebCoins.updateCoinEtherWithdrawHistoryId(coinId, historyId)
                } else if (coinType == "dai") {
                    bitwebCoins.updateCoinDaiWithdrawHistoryId(coinId, historyId)
                } else if (coinType == "btc") {
                    bitwebCoins.updateCoinBtcWithdrawHistoryId(coinId, historyId)
                } else {
                    console.log('exception > updateWithWithdrawHistory')
                }
            })
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })
}

function getByCoinId(coinId) {

    return new Promise((resolve, reject) => {
        db.connectDB()
            .then(() => bitwebCoins.getCoinById(coinId))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })
}

function updateTotalCoin(coinId, data) {
    return new Promise((resolve , reject) => {

        db.connectDB()
            .then(() => bitwebCoins.updateCoinById(coinId, data))
            .then((result) => {
                console.log('result=>' , result);
                resolve(result)
            }).catch((err) => {
            reject(err)
        })
    })
}

function updateCoinHistoryStatusById(historyId, history) {
    return new Promise((resolve , reject) => {

        var data = {};
        data['completeDate'] = util.formatDate(new Date().toLocaleString('ko-KR'))
        data['status'] = true;
        data['completedPrice'] = history.completedPrice;

        db.connectDB()
            .then(() => {

                if (history['type'] == "ether_withdraw") {
                    bitwebCoins.updateCoinEtherWithdrawHistoryById(historyId, data)
                        .then((result) => {
                            console.log('result=>' , result);
                            resolve(result)
                        }).catch((err) => {
                        reject(err)
                    })
                } else if (history['type'] == "dai_withdraw") {
                    bitwebCoins.updateCoinDaiWithdrawHistoryById(historyId, data)
                        .then((result) => {
                            console.log('result=>' , result);
                            resolve(result)
                        }).catch((err) => {
                        reject(err)
                    })
                } else if (history['type'] == "btc_withdraw") {
                    bitwebCoins.updateCoinBtcWithdrawHistoryById(historyId, data)
                        .then((result) => {
                            console.log('result=>' , result);
                            resolve(result)
                        }).catch((err) => {
                        reject(err)
                    })
                }
            })

    })
}

function updateCoinHistoryWithdrawStatusById(coinType, historyId, withdrawHistoryId) {
    return new Promise((resolve , reject) => {

        var data = {};
        data['withdraw_history_id'] = withdrawHistoryId;

        db.connectDB()
            .then(() => {

                if (coinType == "btc") {
                    bitwebCoins.updateCoinBtcWithdrawStatus(historyId, data)
                        .then((result) => {
                            console.log('result=>' , result);
                            resolve(result)
                        }).catch((err) => {
                        reject(err)
                    })
                } else if (coinType == "eth") {
                    bitwebCoins.updateCoinEtherWithdrawStatus(historyId, data)
                        .then((result) => {
                            console.log('result=>' , result);
                            resolve(result)
                        }).catch((err) => {
                        reject(err)
                    })
                }
            })

    })
}

function updateCoinDaiWithdrawHistory(historyId, data) {
    return new Promise((resolve, reject) => {
        db.connectDB()
            .then(() => {
                bitwebCoins.updateCoinDaiWithdrawHistoryById(historyId, data)
                    .then((result) => {
                        //console.log('result=>' , result);
                        resolve(result)
                    }).catch((err) => {
                    reject(err)
                })
            })
    });
}

exports.listCoins = listCoins;
exports.createCoinWithdrawHistoryByCoinId = createCoinWithdrawHistoryByCoinId;
exports.updateWithWithdrawHistory = updateWithWithdrawHistory;
exports.getByCoinId = getByCoinId;
exports.updateTotalCoin = updateTotalCoin;
exports.updateCoinHistoryStatusById = updateCoinHistoryStatusById;
exports.updateCoinHistoryWithdrawStatusById = updateCoinHistoryWithdrawStatusById;
exports.getCoinDaiWithdrawHistory = getCoinDaiWithdrawHistory;
exports.updateCoinDaiWithdrawHistory = updateCoinDaiWithdrawHistory;