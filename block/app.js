App = {
  web3Provider: null,
  contracts: {},

  init: function() {
    return App.initWeb3();
    
  },

  initWeb3: function() {
    
    if (typeof web3 !== 'undefined') {
      App.web3Provider = web3.currentProvider;
      



    } else {
      //App.web3Provider = new Web3(new Web3.providers.HttpProvider("https://mainnet.infura.io/bb999642de9b491b9929112c06f97350"));
      App.web3Provider = new Web3(new Web3.providers.HttpProvider("https://ropsten.infura.io/0c938ce8db2547fdbb12f73b5d500aca"));
      //App.web3Provider = new Web3.providers.HttpProvider('http://127.0.0.1:8545');
      //App.web3Provider = new Web3.providers.HttpProvider('https://ropsten.infura.io/0c938ce8db2547fdbb12f73b5d500aca');
 
      //App.web3Provider =  new Web3(new Web3.providers.HttpProvider('https://rinkeby.infura.io:443')) ;
      //web3 = new Web3(App.web3Provider);
    }
      return App.initContract();

     
  }, 
  initContract: function() {
    $.ajaxSetup({async: false});  
    $.getJSON('TokenCrowdsale.json', function(data) {      
      var TokenCrowdsaleArtifact = data;
      App.contracts.TokenCrowdsale = TruffleContract(TokenCrowdsaleArtifact);
      App.contracts.TokenCrowdsale.setProvider(App.web3Provider);
      
      return App.getRaisedFunds(), App.getRaisedToken(), App.getGoalFunds(), App.getEndTime(), App.isFinalized(), App.getTokenPrice100(), App.isGoalReached(), App.getEthRefundValue(), App.gethasClosed(), App.isLockup();
    });
    /*
    $.getJSON('RefundVault.json', function(data) {
      var RefundVaultArtifact = data;
      App.contracts.RefundVault = TruffleContract(RefundVaultArtifact);
      App.contracts.RefundVault.setProvider(App.web3Provider);
    });
    */
   $.getJSON('Test.json', function(data) {
    var destArtifact = data;
    App.contracts.Test = TruffleContract(destArtifact);
    App.contracts.Test.setProvider(App.web3Provider);
  });

    $.getJSON('PausableToken.json', function(data) {
      var PausableTokenArtifact = data;
      App.contracts.PausableToken = TruffleContract(PausableTokenArtifact);
      App.contracts.PausableToken.setProvider(App.web3Provider);
    });

    return App.bindEvents();


  },

  bindEvents: function() {
 
    $(document).on('click', '#buyTokens', App.handleBuyTokens);
    $(document).on('click', '#bonusToken', App.handleBonusToken);
    $(document).on('click', '#DeleteToken', App.handleDeleteToken);
    $(document).on('click', '#DeleteToken2', App.handleDeleteToken2);
    $(document).on('click', '#ViewOwner', App.handleViewOwner);
    ViewOwner
    $(document).on('click', '#DeleteCrowd', App.handleDeleteCrowd);
    //$(document).on('click', '#getEthBal', App.handleGetEthBalance);
 
    $(document).on('click', '#claimRefund', App.handleClaimRefund);

     
    //$(document).on('click', '#createButton', App.handleCreate);
    $(document).on('click', '#finalizeButton', App.handleFinalize);
    $(document).on('click', '#addButton', App.handleAdd);
    $(document).on('click', '#burnButton', App.handleBurn);
    $(document).on('click', '#getTotalSupplyButton', App.handlGetotalSupply);
    $(document).on('click', '#getBalanceButton', App.handleGetBalance);
    //$(document).on('click', '#getEscrowBalanceButton', App.handlGetEscrowBalance);
    

    //$(document).on('click', '#addWalletButton', App.handleAddWallet);
    
    $(document).on('click', '#setRateButton', App.handleSetRate);
    $(document).on('click', '#getRateButton', App.handleGetRate);

 
    $(document).on('click', '#setSoftCapButton', App.handleSetSoftCap);
    $(document).on('click', '#getSoftCapButton', App.handleGetSoftCap);

    
    $(document).on('click', '#setHardCapButton', App.handleSetHardCap);
    $(document).on('click', '#getHardCapButton', App.handleGetHardCap);

    
    $(document).on('click', '#setTimeButton', App.handleSetTime);
    $(document).on('click', '#getTimeButton', App.handleGetTime);

    $(document).on('click', '#deleteButton', App.handleDelete);
    $(document).on('click', '#forcefinalizeButton', App.handleForceFinalize);
    $(document).on('click', '#sendEthButton', App.handleSendEth);

    $(document).on('click', '#sendToken', App.handlesendToken);

    $(document).on('click', '#onPause', App.handleOnPause);
    $(document).on('click', '#offPause', App.handleOffPause);

    $(document).on('click', '#lock', App.handleLock);
    $(document).on('click', '#unlock', App.handleUnlock);
    $(document).on('click', '#timelock', App.handleTimelock);
  
  $(document).on('click', '#now', App.handleNow);
  $(document).on('click', '#showtimelock', App.handleshowTimelock);
  $(document).on('click', '#showtimelockvalue', App.handleshowTimelockValue);
  $(document).on('click', '#showtokenvalue', App.handlshowtokenvalue);

    $(document).on('click', '#checkAddress', App.handleCheckAddress);

  },
/*
  handleCreate: function(event) {
    return App.initWeb3();
  },
*/

  
  handleBuyTokens: function send() {
    var amount = parseInt($('#Balance').val());

  
    
    
    

    App.contracts.TokenCrowdsale.deployed().then(function(instance) {
      crowdsale = instance;
      var crowdsaleContractAddress = crowdsale.address;
      crowdsale.rate().then(function(rate){
        var rate = rate;

        web3.eth.sendTransaction({
          from: web3.eth.coinbase,
          to: crowdsaleContractAddress,
          value: web3.toWei(amount/rate, 'ether'),//web3.toWei(amount/rate, 'ether'),
          gas : 150000

        },function(error, result) {
          if (!error) {
            console.log('from : ' + web3.eth.coinbase);
            console.log('to : ' + crowdsaleContractAddress);
          } else {
            console.log("aaa"+error.message);
          }
          });
      });
    }).catch(function(err) {
      console.log("ERR");
    });
  },


  
  handleCheckAddress: function () {

    var isAddress = function (address) {
      // function isAddress(address) {
          if (!/^(0x)?[0-9a-f]{40}$/i.test(address)) { 
          return false;
      } else if (/^(0x)?[0-9a-f]{40}$/.test(address) || /^(0x)?[0-9A-F]{40}$/.test(address)) {
 
          return true;
      } else { 
          return isChecksumAddress(address);
      }
  }
  
  
  var isChecksumAddress = function (address) { 
      address = address.replace('0x','');
      var addressHash = web3.sha3(address.toLowerCase());
      for (var i = 0; i < 40; i++ ) { 
          if ((parseInt(addressHash[i], 16) > 7 && address[i].toUpperCase() !== address[i]) || (parseInt(addressHash[i], 16) <= 7 && address[i].toLowerCase() !== address[i])) {
              return false;
          }
      }
      return true;
  }
    var adr = $('#Address').val(); 
   console.log(isAddress(adr)); 

    },
  
    


  handlesendToken: function () {
    /*
    var to = $('#To').val();
    var amount = parseInt($('#Amount').val());
    console.log('amount: ' + amount);
    App.contracts.TokenCrowdsale.deployed().then(function(instance) {
      crowdsale = instance;
      return crowdsale.token();
      }).then(function(address){
        var token_contract_address = address;
        console.log('Token contract address: ' + token_contract_address);

        token_contract = App.contracts.PausableToken.at(token_contract_address);

        return token_contract.transfer(to,amount);
      }).then(function(result) {
        console.log(result);
      
      }).catch(function(err) {
        console.log(err.message);
      });
      */

 


     var to = $('#To').val();
     var amount = parseInt($('#Amount').val());
     console.log('to: ' + to);
     console.log('amount: ' + amount);
     App.contracts.TokenCrowdsale.deployed().then(function(instance) {
       crowdsale = instance;
       console.log(crowdsale.token)
       return crowdsale.mintToken(to,amount);
        }).catch(function(err) {
         console.log(err.message);
       });

  },
  

/*
handlesendToken: function () {
  var to = $('#To').val();
  var amount = parseInt($('#Amount').val());
  console.log('Send token ');
  console.log(amount);
  App.contracts.TokenCrowdsale.deployed().then(function(instance) {
      crowdsale = instance;
      return crowdsale.transferToken(to,amount);
  }).then(function(result){
    console.log(result);
    }).catch(function(err) {
        console.log(err.message);
      });
},
  */

  
/*
  handleGetEthBalance: function(){
  console.log('Getting Eth');
  var bal = $('#ethAddress').val();
  console.log(bal);
  App.contracts.TokenCrowdsale.deployed().then(function(instance) {
      crowdsale = instance;
      return crowdsale.depositsAt();
  }).then(function(result){
    console.log(result);
    
    $('#ethBalance').text(result);
    }).catch(function(err) {
        console.log(err.message);
      });
},
*/


handleDeleteToken2: function(){
  console.log('delete  token2');
  console.log(App.contracts.Test);
   
  App.contracts.Test.deployed().then(function(instance) {
      crowdsale = instance;
      return crowdsale.destroyToken();
  }).then(function(result){
    console.log(result);
    

    }).catch(function(err) {
        console.log(err.message);
      });
},



handleDeleteToken: function(){
    console.log('delete  token');
     
    App.contracts.TokenCrowdsale.deployed().then(function(instance) {
        crowdsale = instance;
        return crowdsale.destroyToken();
    }).then(function(result){
      console.log(result);
      

      }).catch(function(err) {
          console.log(err.message);
        });
  },


handleDeleteCrowd: function(){
  console.log('delete crowd');
   
  App.contracts.TokenCrowdsale.deployed().then(function(instance) {
      crowdsale = instance;
      return crowdsale.destroyCrowdsale();
  }).then(function(result){
    console.log(result);
    

    }).catch(function(err) {
        console.log(err.message);
      });
},


  handleBonusToken: function(){
    console.log('change owner');
    
    var to = $('#Balance').val();
    
    App.contracts.TokenCrowdsale.deployed().then(function(instance) {
        crowdsale = instance;
        return crowdsale.onwerChange(to);
    }).then(function(result){
      console.log(result);      
    }).catch(function(err) {
        console.log(err.message);
        });
  },




  getBalance: function() {
    console.log('Getting balances...');
    App.contracts.TokenCrowdsale.deployed().then(function(instance) {
      crowdsale = instance;
      return crowdsale.token();
      }).then(function(address){
        var token_contract_address = address;
        console.log('Token contract address: ' + token_contract_address);
        token_contract = App.contracts.MintableToken.at(token_contract_address);
        return token_contract.balanceOf(web3.eth.coinbase);
      }).then(function(balance) {
        console.log(balance);
        tokenBalance = balance; 
        console.log(tokenBalance);
        $('#tokenBalance').text(tokenBalance.toString(10));
      }).catch(function(err) {
        console.log(err.message);
      });
  },

  getRaisedFunds: function(){
    console.log('Getting raised fund...');

    console.log(web3.eth.defaultAccount);
    if (web3.eth.defaultAccount !== 'undefined')
    {
      console.log('ok...');
    }else{
      console.log('no...');
    }
    
    App.contracts.TokenCrowdsale.deployed().then(function(instance) {
        crowdsale = instance;
        return crowdsale.weiRaised();
    }).then(function(result){
      EthRaised = Math.round(1000*result/1000000000000000000)/1000;
      $('#ETHRaised').text(EthRaised.toString(10));
      }).catch(function(err) {
          console.log(err.message);
        });
  },

  getRaisedToken: function(){
    console.log('Getting raised token...');

  },

  getGoalFunds: function(){
    console.log('Getting goal funds...');
    App.contracts.TokenCrowdsale.deployed().then(function(instance) {
        crowdsale = instance;
        return crowdsale.goal();
    }).then(function(result){
      console.log(result);
      EthGoal = Math.round(1000*result/1000000000000000000)/1000;
      $('#ETHGoal').text(EthGoal.toString(10));
      }).catch(function(err) {
          console.log(err.message);
        });
  },
  getTokenPrice100: function(){
    console.log('Getting 100 token price in ETH...');
    App.contracts.TokenCrowdsale.deployed().then(function(instance) {
        crowdsale = instance;
        return crowdsale.rate();
    }).then(function(result){
      tokenPrice100 = Math.round(1000*100/result)/1000;
      $('#100TokenPrice').text(tokenPrice100.toString(10));
      }).catch(function(err) {
          console.log(err.message);
        });
  },




  getEndTime: function(){
    console.log('Getting endtime...');
    App.contracts.TokenCrowdsale.deployed().then(function(instance) {
        crowdsale = instance;
        return crowdsale.closingTime();
    }).then(function(result){
      endTime = new Date(result.c[0]*1000);      
      $('#EndTime').text(endTime);
      }).catch(function(err) {
          console.log(err.message);
        });
  },

  gethasClosed: function(){
    console.log('Getting hasClosed()...');
    App.contracts.TokenCrowdsale.deployed().then(function(instance) {
      crowdsale = instance;
      return crowdsale.hasClosed();
  }).then(function(result){
    $('#hasClosed').text(result);
    }).catch(function(err) {
        console.log(err.message);
      });
  },

  //TODO: rewrite to show token contract address rather than crowdsale
  getTokenContractAddress: function(){
    console.log('Getting token contract address...');
    App.contracts.TokenCrowdsale.deployed().then(function(instance) {
      crowdsale = instance;
      crowdsaleAddress = crowdsale.address
      console.log('TokenCrowdsale Address: ' + crowdsaleAddress);
      return crowdsale.token()
    }).catch(function(err) {
      console.log(err.message);
      }).then(function(token_address){
      tokenContractAddress = token_address;
      console.log('Token Crowdsale Address: ' + tokenContractAddress);
      $('#TokenContractAddress').text(tokenContractAddress);
      });
  },

  handleAdd: function(event) {
    event.preventDefault();
    console.log('add whilteList...');
    var address = $('#Address').val();
    console.log('address : ' + address);
    App.contracts.TokenCrowdsale.deployed().then(function(instance) {
      crowdsale = instance;
      return crowdsale.addAddressToWhitelist(address);
  }).then(function(result){
      console.log("success");
    }).catch(function(err) {
        console.log(err.message);
      });

   },

   handleBurn: function(event) {
    event.preventDefault();
    console.log('Burn Token...');
    var address = $('#burnAddress').val();
    var value = $('#burnValue').val();
    console.log('burnAddress : ' + address);
    App.contracts.TokenCrowdsale.deployed().then(function(instance) {
      crowdsale = instance;
      return crowdsale.burnToken(address, value);
  }).then(function(result){
      console.log("success");
    }).catch(function(err) {
        console.log(err.message);
      });

   },



  handleFinalize: function(event) {
    event.preventDefault();
    console.log('Finalizing...');
    var crowdsale;
      App.contracts.TokenCrowdsale.deployed().then(function(instance) {
        crowdsale = instance;
        crowdsale.finalize();
      });
  },

  handleForceFinalize: function(event) {
    event.preventDefault();
    console.log('Force Finalizing...');
    var crowdsale;
      App.contracts.TokenCrowdsale.deployed().then(function(instance) {
        crowdsale = instance;
        crowdsale.forceFinalize();
      });
  },
  handleSendEth: function(event) {
    event.preventDefault();
    console.log('Send Eth...');
    var crowdsale;
      App.contracts.TokenCrowdsale.deployed().then(function(instance) {
        crowdsale = instance;
        crowdsale.sendEth();
      });
  },


  isFinalized: function(){
    console.log('Check  Finalization status...');
    App.contracts.TokenCrowdsale.deployed().then(function(instance) {
        crowdsale = instance;
        return crowdsale.isFinalized();
    }).then(function(result){
      $('#isFinalized').text(result);
      }).catch(function(err) {
          console.log(err.message);
        });
  },

  isLockup: function(){
    console.log('Check  Lockup status...');
    App.contracts.TokenCrowdsale.deployed().then(function(instance) {
        crowdsale = instance;
        return crowdsale.isLockup();
    }).then(function(result){
      console.log(result);
      $('#isLockup').text(result);
      }).catch(function(err) {
          console.log(err.message);
        });
  },


  isGoalReached: function(){
    console.log('Check  Goal Reached status...');
    App.contracts.TokenCrowdsale.deployed().then(function(instance) {
        crowdsale = instance;
        return crowdsale.goalReached();
    }).then(function(result){
      $('#isGoalReached').text(result);
      }).catch(function(err) {
          console.log(err.message);
        });

  },
 
  handleClaimRefund: function(event) {
    event.preventDefault();
    console.log('Claiming Refund ...');
    App.contracts.TokenCrowdsale.deployed().then(function(instance) {
        crowdsale = instance;
        return crowdsale.claimRefund();
    }).then(function(result){
          console.log("success");
      }).catch(function(err) {
          console.log(err.message);
        });
  },

  
  getEthRefundValue: function() {
    console.log('Getting Eth Refund value...');
    App.contracts.TokenCrowdsale.deployed()
    .then(function(instance) {
      var crowdsale = instance;
      return crowdsale.vault()
      }).then(function(vaultaddr){
        var vaultaddr = vaultaddr;
        var vault = App.contracts.RefundVault.at(vaultaddr)
        return vault.deposited(web3.eth.coinbase)
        }).then(function(balance) {
          ethRefundValue = Math.round(1000*balance/1000000000000000000)/1000;
          $('#ethRefundValue').text(ethRefundValue.toString(10));
          }).catch(function(err) {
            console.log(err.message);
            });
  },
  
  
  handleViewOwner: function(event) {    
    App.contracts.TokenCrowdsale.deployed().then(function(instance) {
      crowdsale = instance;
      return crowdsale.token();
      }).then(function(address){
        var token_contract_address = address;
        console.log('Token contract address: ' + token_contract_address);
        token_contract = App.contracts.MintableToken.at(token_contract_address);
        return token_contract.owner();
      }).then(function(balance) {
        console.log(balance);
      }).catch(function(err) {
        console.log(err.message);
      });
  },

  handleGetRate: function(event) {    
    event.preventDefault();
    console.log('Getting rate ...');
    App.contracts.TokenCrowdsale.deployed().then(function(instance) {      
        crowdsale = instance;        
        return crowdsale.rate();
    }).then(function(rate){
      console.log(rate);
        $('#lblRate').text(rate.c[0]);
      }).catch(function(err) {
          console.log(err.message);
        });
  },
  handleSetRate: function(event) {    
    event.preventDefault();
    console.log('Setting rate ...');
    var rate = $('#Rate').val();
    App.contracts.TokenCrowdsale.deployed().then(function(instance) {      
        crowdsale = instance;        
        return crowdsale.setRate(rate);
    }).then(function(result){
          console.log("success");
      }).catch(function(err) {
          console.log(err.message);
        });
  },

  handleGetSoftCap: function(event) {    
    event.preventDefault();
    console.log('Getting softCap ...');
    App.contracts.TokenCrowdsale.deployed().then(function(instance) {      
        crowdsale = instance;        
        return crowdsale.goal();
    }).then(function(goal){
          console.log(goal.c[0]);
          $('#lblSoftCap').text(goal.c[0]);
      }).catch(function(err) {
          console.log(err.message);
        });
  },
  handleSetSoftCap: function(event) {    
    event.preventDefault();
    console.log('Setting softCap ...');
    var softCap = web3.toWei(($('#SoftCap').val(),'ether'));
    App.contracts.TokenCrowdsale.deployed().then(function(instance) {      
        crowdsale = instance;        
        return crowdsale.setSoftCap(softCap);
    }).then(function(result){
          console.log("success");
      }).catch(function(err) {
          console.log(err.message);
        });
  },

  handleGetHardCap: function(event) {    
    event.preventDefault();
    console.log('Getting hardCap ...');
    App.contracts.TokenCrowdsale.deployed().then(function(instance) {      
        crowdsale = instance;        
        return crowdsale.cap();
    }).then(function(cap){
          console.log(cap);
          $('#lblHardCap').text(cap.c[0]);
          console.log("success");
      }).catch(function(err) {
          console.log(err.message);
        });
  },
  handleSetHardCap: function(event) {    
    event.preventDefault();
    console.log('Setting hardCap ...');
    var hardCap = web3.toWei(($('#HardCap').val(),'ether'));
    App.contracts.TokenCrowdsale.deployed().then(function(instance) {      
        crowdsale = instance;        
        return crowdsale.setHardCap(hardCap);
    }).then(function(result){
          console.log("success");
      }).catch(function(err) {
          console.log(err.message);
        });
  },

  handleGetTime: function(event) {    
    event.preventDefault();
    console.log('Setting hardCap ...');
    App.contracts.TokenCrowdsale.deployed().then(function(instance) {      
        crowdsale = instance;        
        return crowdsale.openingTime();
    }).then(function(startTime){
          console.log(startTime);
          $('#lblStartTime').text(startTime);
          console.log("success");
          return crowdsale.closingTime();
      }).then(function(endTime){
        console.log(endTime);
        $('#lblEndTime').text(endTime);
        console.log("success");
      }).catch(function(err) {
          console.log(err.message);
        });
  },

  handleSetTime: function(event) {    
    event.preventDefault();
    console.log('Setting Time ...');
    var start = $('#txtStart').val();
    var end = $('#txtEND').val();
    App.contracts.TokenCrowdsale.deployed().then(function(instance) {      
        crowdsale = instance;        
        return crowdsale.setTime(start, end);
    }).then(function(result){
          console.log("success");
      }).catch(function(err) {
          console.log(err.message);
        });
  },

  handleDelete: function(event) {    
    event.preventDefault();
    console.log('Setting Delete ...');
    App.contracts.TokenCrowdsale.deployed().then(function(instance) {      
        crowdsale = instance;        
        return crowdsale.destroy();
    }).then(function(result){
          console.log("success");
      }).catch(function(err) {
          console.log(err.message);
        });
  },

  handleGetBalance: function(event) {    
    event.preventDefault();
    console.log('Getting balance ...');
    App.contracts.TokenCrowdsale.deployed().then(function(instance) {      
        crowdsale = instance;        
        return crowdsale.getBalance();
    }).then(function(bal){
          console.log(bal);
          $('#lblBalance').text(bal);
          console.log("success");
      }).catch(function(err) {
          console.log(err.message);
        });
  },
/*
  handlGetEscrowBalance: function(event) {    
    event.preventDefault();
    console.log('Getting Escrow ..Balance.');
    App.contracts.TokenCrowdsale.deployed().then(function(instance) {      
        crowdsale = instance;        
        return crowdsale.getBalance();
    }).then(function(bal){
          console.log(bal);
          $('#lblEscrowBalance').text(bal);
          console.log("success");
      }).catch(function(err) {
          console.log(err.message);
        });
  },
*/

  handlGetotalSupply: function(event) {    
    event.preventDefault();
    console.log('Getting totalSupply ...');
    App.contracts.TokenCrowdsale.deployed().then(function(instance) {      
        crowdsale = instance;        
        return crowdsale.getTotalSupply();
    }).then(function(result){
          console.log(result);                      
          $('#lbltotalSupply').text(result.toString(10));
      

          console.log("success");
      }).catch(function(err) {
          console.log(err.message);
        });
  },

  handleOnPause: function(event) {    
    console.log("handleOnPause..");
    event.preventDefault();
    App.contracts.TokenCrowdsale.deployed().then(function(instance) {
      crowdsale = instance;
      return crowdsale.pause();
    }).then(function(result){
      console.log(result);
                
      console.log("success");
  }).catch(function(err) {
      console.log(err.message);
    });
},



  handleOffPause: function(event) {    
    console.log("handleOffPause..");
    event.preventDefault();
    App.contracts.TokenCrowdsale.deployed().then(function(instance) {
      crowdsale = instance;
      return crowdsale.unpause();
    }).then(function(result){
      console.log(result);
      
      console.log("success");
  }).catch(function(err) {
      console.log(err.message);
    });
},

handleLock: function(event) {    
  var adr = $('#lockAddress').val(); 

  console.log("handleLock.aa.");
  event.preventDefault();
  App.contracts.TokenCrowdsale.deployed().then(function(instance) {
    crowdsale = instance;
    console.log(instance);
    return crowdsale.lock(adr);
  }).then(function(result){
    console.log(result);
    
    console.log("success");
}).catch(function(err) {
    console.log(err.message);
  });
},

handleUnlock: function(event) {    
  var adr = $('#lockAddress').val(); 
  console.log("handleUnlock..");
  event.preventDefault();
  App.contracts.TokenCrowdsale.deployed().then(function(instance) {
    crowdsale = instance;
    return crowdsale.unlock(adr);
  }).then(function(result){
    console.log(result);
    
    console.log("success");
}).catch(function(err) {
    console.log(err.message);
  });
},

handleTimelock: function(event) {    
  var adr = $('#lockAddress').val(); 
  var time = $('#time').val(); 
  var value = $('#value').val(); 
  console.log("handleTimelock..!!!");
  event.preventDefault();
  App.contracts.TokenCrowdsale.deployed().then(function(instance) {
    crowdsale = instance;
    return crowdsale.timelock(adr,time,value);
  }).then(function(result){
    console.log(result);
    
    console.log("success");
}).catch(function(err) {
    console.log(err.message);
  });
},

handleNow: function(event) {    
  console.log("handleTimelock..");
  event.preventDefault();
  App.contracts.TokenCrowdsale.deployed().then(function(instance) {
    crowdsale = instance;
    return crowdsale.now();
  }).then(function(result){
    console.log(result);
    
    console.log("success");
}).catch(function(err) {
    console.log(err.message);
  });
},

handleshowTimelock: function(event) {    
  var adr = $('#lockAddress').val(); 
  
  console.log("handleUnlock..");
  event.preventDefault();
  App.contracts.TokenCrowdsale.deployed().then(function(instance) {
    crowdsale = instance;
    return crowdsale.showTimeLock(adr);
  }).then(function(result){
    console.log(result);
    
    console.log("success");
}).catch(function(err) {
    console.log(err.message);
  });
},

handleshowTimelockValue: function(event) {    
  var adr = $('#lockAddress').val(); 
   
  event.preventDefault();
  App.contracts.TokenCrowdsale.deployed().then(function(instance) {
    crowdsale = instance;
    return crowdsale.showTimeLockValue(adr);
  }).then(function(result){
    console.log(result);
    
    console.log("success");
}).catch(function(err) {
    console.log(err.message);
  });
},

handlshowtokenvalue: function(event) {    
  var adr = $('#lockAddress').val(); 
    
  App.contracts.TokenCrowdsale.deployed().then(function(instance) {
    crowdsale = instance;
    return crowdsale.showTokenValue(adr);
  }).then(function(result){
    console.log(result);
    
    console.log("success");
}).catch(function(err) {
    console.log(err.message);
  });
},








  

 
  

};

$(function() {
  $(window).load(function() {
    App.init();
  });
});